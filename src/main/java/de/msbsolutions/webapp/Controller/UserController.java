package de.msbsolutions.webapp.Controller;

import java.util.LinkedList;
import java.util.List;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import de.msbsolutions.webapp.Models.User.User;
import de.msbsolutions.webapp.Repositories.MongoRepository;

@Controller
public class UserController {

    private MongoRepository mongoRepository;
	
	public UserController() {
		mongoRepository = new MongoRepository();
		
	}

    @GetMapping("/users")
    @ResponseBody
    public List<User> getUser() {
    	List<User> result = new LinkedList<>();
    	
    	/*MultivaluedMap<String, String> queryParams = ui.getQueryParameters();
    	MultivaluedMap<String, String> pathParams = ui.getPathParameters();
    	
    	if(queryParams.containsKey("id")) {
    		System.out.println("get called => ID = " + queryParams.getFirst("id"));
    		result.add(mongoRepository.getOne(queryParams.getFirst("id")));
    		return result;    		
    	}
    	//queryParams.forEach((k,v)->System.out.println("Itemquery : " + k + " Count : " + v));
    	//pathParams.forEach((k,v)->System.out.println("Itempath : " + k + " Count : " + v));
        */
        System.out.println("getAll called");
    	result = mongoRepository.getAll();
    	return result; 
    }


    @PostMapping("/users")
    @ResponseBody
    public User users(@RequestParam(name="user", required=false, defaultValue="World") String name, User user) {
        System.out.println("Post called -> \n" + user.toString());
    	User newUser = User.importUser(user);
    	mongoRepository.create(newUser);
    	System.out.println("User created: \n" + newUser.toString());
    	return newUser;
    }

}