package de.msbsolutions.webapp.Repositories;

import static org.bson.codecs.configuration.CodecRegistries.fromProviders;
import static org.bson.codecs.configuration.CodecRegistries.fromRegistries;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

import org.bson.codecs.configuration.CodecRegistry;
import org.bson.codecs.pojo.PojoCodecProvider;

import com.mongodb.Block;
import com.mongodb.MongoClientSettings;
import com.mongodb.client.FindIterable;
import com.mongodb.client.MongoClient;
import com.mongodb.client.MongoClients;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import com.mongodb.client.model.Filters;
import de.msbsolutions.webapp.Models.User.*;

public class MongoRepository implements IRepository<User>{
	
	String uri;
	MongoClient mongoClient;
	MongoDatabase database;
	MongoCollection<User> collection;
	CodecRegistry pojoCodecRegistry;

	
	public MongoRepository()
	{		
		uri = "mongodb+srv://martin:?18daCA01!@invoicetest-hb06x.mongodb.net/test?retryWrites=true";
		mongoClient = MongoClients.create(uri);
		pojoCodecRegistry = fromRegistries(MongoClientSettings.getDefaultCodecRegistry(),
				fromProviders(PojoCodecProvider.builder().automatic(true).build()));
		database = mongoClient.getDatabase("msb_backend").withCodecRegistry(pojoCodecRegistry);
		collection = database.getCollection("users", User.class);	
		System.out.println("Mongorepository created");
	}
	
	@Override
	public void create(User user) 
	{	
		collection.insertOne(user);
		mongoClient.close();
	}

	@Override
	public User getOne(String queryParams) {
		FindIterable<User> result = collection.find(Filters.eq(queryParams));
		mongoClient.close();
		return result.first();
	}
	
	@Override
	public List<User> getMany(Arrays params) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void update(User t) {
		// TODO Auto-generated method stub		
	}

	@Override
	public void delete(String id) {
		// TODO Auto-generated method stub		
	}

	public List<User> getAll() {
		final List<User> result = new LinkedList<User>();		
		Block<User> block = new Block<User>()
		{
		    @Override
		    public void apply(final User user) {
		        result.add(user);
		    }
		};
		collection.find().forEach(block);
		mongoClient.close();
		return result;		
	}
	
}
