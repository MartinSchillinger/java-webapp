package de.msbsolutions.webapp.Repositories;

import java.util.Arrays;
import java.util.List;

public interface IRepository<T>  {
	public void create(T t);
	public T getOne(String t);
	public List<T> getMany(Arrays params);
	public void update(T t);
	public void delete(String id);	
}
