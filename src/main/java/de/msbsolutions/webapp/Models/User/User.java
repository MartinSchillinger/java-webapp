package de.msbsolutions.webapp.Models.User;

import java.util.List;
import java.util.UUID;

public class User
{
	private String id;
	private String firstName, lastName, password, email;
	private List<UserMeta> usermeta;
	
	public User() {
		
	}
	
	public static User create(String firstName, String lastName, String email, String password)
	{
		User result = new User();
		result.setId(UUID.randomUUID().toString());
		result.setEmail(email);
		result.setFirstName(firstName);
		result.setLastName(lastName);
		result.setPassword(password);
		return result;
	}
	
	public static User importUser(User user)
	{
		User result = new User();
		result.setId(UUID.randomUUID().toString());
		result.setEmail(user.getEmail());
		result.setFirstName(user.getFirstName());
		result.setLastName(user.getLastName());
		result.setPassword(user.getPassword());
		return result;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}
	
	public String toString() {
		return "ID: " + id + "\nFirst Name: " + firstName + "\nLast Name: " + lastName + "\nEmail: " + email + "\nPassword: " + password;		
	}
	
	public class UserMeta{
		public int id;
		public String key, value;
	}
}


